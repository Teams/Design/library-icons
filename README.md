# Library Icon

Icon style for libraries and system components. Unlike applications, libraries and system components do not have striking visual identity coming from the [fullcolor app icon](https://developer.gnome.org/hig/guidelines/app-icons.html). A flat color/gradient pill is used for libraries and components.

![Tracker](library-icons/tracker-r.svg)
![LibPortal](library-icons/libportal-r.svg)
![LibRSVG](library-icons/librsvg-r.svg)

## Steps to produce a library icon

- Design a symbolic, export using [Symbolic Preview](https://flathub.org/apps/details/org.gnome.design.SymbolicPreview)
- Pick a solid color or gradinent to go along with the symbol using [Emblem](https://flathub.org/apps/details/org.gnome.design.Emblem)
- Export and commit the resulting SVG (rectangular).

## Round vs Square

![Mutter](library-icons/mutter.svg)
![Mutter](library-icons/mutter-r.svg)

While the desired output silhouette is round, many platforms will benefit from the exported shape to be square and provide the rounding through CSS or similar.